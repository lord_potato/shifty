#include <Arduino.h>
#include "shifty.h"

shiftRegister::shiftRegister(uint8_t dataPin, uint8_t clockPin, uint8_t latchPin) {
    pins.dataPin = dataPin;
    pins.clockPin = clockPin;
    pins.latchPin = latchPin;
    pinMode(dataPin,OUTPUT);
    pinMode(clockPin,OUTPUT);
    pinMode(latchPin,OUTPUT);
    digitalWrite(pins.latchPin,LOW);
    digitalWrite(pins.clockPin,LOW);
    digitalWrite(pins.dataPin,LOW);
}

void shiftRegister::setOrder(uint8_t bitOrder){
    order = bitOrder;
}

void shiftRegister::latch(uint16_t delayMicros){
    digitalWrite(pins.latchPin,LOW);
    delayMicroseconds(delayMicros);
    digitalWrite(pins.latchPin,HIGH);
}

void shiftRegister::shift(uint8_t data){
    for (uint8_t i = 0; i < 8; i++){
        if (order == 0)
            digitalWrite(pins.dataPin, !!(data & (1 << i)));
        else    
            digitalWrite(pins.dataPin, !!(data & (1 << (7 - i))));

        digitalWrite(pins.clockPin, HIGH);
        digitalWrite(pins.clockPin, LOW);
    }
}
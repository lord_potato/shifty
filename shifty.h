#ifndef SHIFTY_INCLUDE
#define SHIFTY_INCLUDE

#include <inttypes.h>

class shiftRegister {
    public:
        shiftRegister(uint8_t dataPin, uint8_t clockPin, uint8_t latchPin);
        void setOrder(uint8_t bitOrder);
        void shift(uint8_t data);
        void latch(uint16_t delayMicros = 100);
    private:
        struct shiftreg_pins{
            uint8_t dataPin;
            uint8_t clockPin;
            uint8_t latchPin;
        } pins;
        uint8_t order = 0;
};

#endif